from string import ascii_uppercase
from itertools import tee, zip_longest
import sys

class Rule:
  def __init__(self, o, i):
    self.o = o
    self.i = i
    self.f1 = False
    self.f2 = False

  def __repr__(self):
    return "{} → {}".format(", ".join(map(chr, self.i)), chr(self.o))

def error(cond, s):
  if cond:
    sys.stderr.write(s)
    sys.stderr.write("\n")
    sys.exit(1)

# Duomenų nuskaitymo funkcija
def read_rules(fname):
  it = (line.strip() for line in open(fname, "r")
        if line and not line.startswith("#"))
  it, n = tee(it, 2)
  it = enumerate(zip_longest(it, n))
  (rules, facts) = ([], [])
  next(n)
  for lineno, (line, peek) in it:
    split = line.split()
    out = split[0]
    i = split[1:]
    error(not all(c in ascii_uppercase for c in i) or
          not out in ascii_uppercase,
          ("Negera eilutė `{}`! " +
           "Tikimasi produkcijos "+
           "(turi būti sudaryta iš didžiųjų ASCII raidžių)"
          ).format(line))
    rules.append(Rule(ord(out), list(map(ord, i))))
    if peek.startswith("+") or peek.startswith("="):
      break;
  for lineno, (line, peek) in it:
    error(not line.startswith("+"),
          ("Negera eilutė `{}`! Tikimasi GDB įrašų " +
           "(eilutės turi prasidėti +)"
          ).format(line))
    for f in line.split()[1:]:
      facts.append(ord(f))
    if peek.startswith("="):
      break;
  (lineno, (line, peek)) = next(it)
  error(not line.startswith("="),
        ("Negera eilutė `{}`! Tikimasi tikslo " +
         "(eilutė turi prasidėti =)"
        ).format(line))
  goals = list(map(ord, line.split()[1:]))
  error(len(goals) != 1,
        "Nurodyta daugiau nei 1 tikslas. Įveskite taisyklę.")
  return (rules, facts, goals[0])

# Pagrindinis išvedimo algoritmas
def apply_rules(rules, facts, goal):
  it = 0
  chain = []
  while True:
    it += 1
    if it == 1: print("{} ITERACIJA".format(it))
    else:
      print("  FAKTAI: {}".format(", ".join(map(chr, rfg[1]))))
      print("\n{} ITERACIJA".format(it))
    # 1 žingsnis: produkcijų perrinkimas
    for ruleno, rule in enumerate(rules):
      if rule.f1 or rule.f2:
        print("  R{}: {} netaikome nes pakelta flag{}."
              .format(ruleno + 1, rule, "1" if rule.f1 else "2"))
        continue
      elif rule.o in facts:
        print(("  R{}: {} netaikome nes egsiztuoja faktas {}. "+
               "Pakeliame flag2.")
              .format(ruleno + 1, rule, chr(rule.o)))
        rule.f2 = True
        continue
      # 2 žingsnis: ar visi antecedentai yra GDB
      if set(rule.i).issubset(facts):
        print("  R{}: {} taikome. Pakeliame flag1.".format(
            ruleno + 1, str(rule)))
        rule.f1 = True
        chain.append(ruleno + 1)
        # 3 žingsnis: konsekventas į GDB, patikriname ar terminalinė būsena
        facts.append(rule.o)
        if goal in facts:
          print("  TIKSLAS IŠVESTAS.")
          return chain
        else:
          break
      else:
        missing = set(rule.i).difference(facts)
        print("  R{}: {} netaikome nes trūksta fakt{} {}."
              .format(ruleno + 1, str(rule),
                      "o" if len(missing) == 1 else "ų",
                      ", ".join(sorted(map(chr, missing)))))
    else:
      # 4 žingsnis: perrinkimas pabaigtas, tačiau terminalinė būsena nerasta
      return None


rfg = read_rules(sys.argv[1])
print("TAISYKLĖS")
for rn, rule in enumerate(rfg[0]):
    print("  R{}:".format(rn+1), str(rule))
print("FAKTAI: {}".format(", ".join(map(chr, rfg[1]))))
print("TIKSLAS: {}\n".format(chr(rfg[2])))
if rfg[2] in rfg[1]:
  print("ATSAKYMAS:\n  TIKSLAS TARP FAKTŲ.")
  sys.exit(0)
else:
  rs = apply_rules(*rfg)
if rs is not None:
  print("\nATSAKYMAS\n  TIKSLAS IŠVEDAMAS.\n  {}"
        .format(", ".join(("R{}".format(r) for r in reversed(rs)))))
else:
  print("\nATSAKYMAS\n  TIKSLAS NEIŠVEDAMAS!")
