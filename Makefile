LATEX_FLAGS = -shell-escape
INS = test1 test2 test3 test4 test5 test5
OUTS = $(patsubst %,%.out,$(INS))
GRAPHS = test1.gv.eps test2.gv.eps test3.gv.eps
IMAGES = VU_L_CL.svg.pdf

TEX_DEPS = lapas.tex gdb.py $(IMAGES) $(INS) $(OUTS) $(GRAPHS) lapas.bbl

all: lapas.pdf

clean:
	rm -rf $(OUTS) $(GRAPHS) *.pdf lapas.bbl lapas.toc lapas.aux lapas.blg lapas.bcf lapas.out lapas.log lapas.run.xml lapas.auxlock _minted-lapas/

lapas.pdf: $(TEX_DEPS) lapas.toc
	lualatex $(LATEX_FLAGS) lapas

lapas.toc: $(TEX_DEPS)
	lualatex $(LATEX_FLAGS) lapas

PATH := ${PATH}:/usr/bin/vendor_perl/
lapas.bbl: lapas.bib lapas.aux lapas.bcf
	biber lapas

lapas.aux lapas.bcf: lapas.tex $(IMAGES)
	lualatex $(LATEX_FLAGS) lapas

%.svg.pdf: %.svg
	inkscape -A $@ $<

%.out: % gdb.py
	python gdb.py $< > $@

%.gv.eps: %.gv
	dot -Teps $< -o$@
