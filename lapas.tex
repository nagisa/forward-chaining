\documentclass[hidelinks,a4paper,12pt]{article}

% TNR compatible font
\usepackage{mathptmx}
% \setmainfont[Ligatures=TeX]{Nimbus Roman}
\usepackage{fontspec}
\newfontfamily\fontlibertine{Linux Libertine O}

% 1.5 line spacing
\usepackage{setspace}
\onehalfspacing
\usepackage[top=2cm, left=3cm, bottom=2cm, right=1.5cm]{geometry}
\usepackage{float}
\usepackage{ragged2e}
\usepackage[nottoc,numbib]{tocbibind}
\usepackage[lithuanian]{babel}
\usepackage{polyglossia}
\setdefaultlanguage{lithuanian}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage[
    pdfencoding=auto,
    psdextra
]{hyperref}
\hypersetup{
pdfauthor={Simonas Kazlauskas},
pdftitle={Tiesioginis išvedimas produkcijų sistemoje},
pdfkeywords={}
}

\usepackage{nameref}
\usepackage{minted}
\usepackage{icomma}
\usepackage{breqn}
\usepackage{url}
\usepackage{indentfirst}
\usepackage{caption}
\usepackage{xcolor}
\usepackage{titlesec}
\titleformat{\section}[block]{\large\scshape\centering}{}{0.5em}{}
\titleformat{\subsection}[block]{\large}{\thesubsection.}{0.5em}{}

\usepackage[backend=biber,date=iso8601,isbn=false,doi=false,url=false,maxnames=99,style=alphabetic]{biblatex}
\renewbibmacro{in:}{}
\renewcommand{\finalnamedelim}{\addspace ir\space}

\setminted{
    frame={lines},
    linenos=true,
    mathescape=true,
    python3=true,
}

\addbibresource{lapas.bib}

\begin{document}
\pagenumbering{gobble}
\begin{titlepage}
    \begin{center}
        \includegraphics[width=0.25\textwidth]{{{VU_L_CL.svg}}}~
        \\[1cm]
        {\large Vilniaus universitetas\\
         Matematikos ir informatikos fakultetas\\
         Informatikos katedra
        }~
        \\[3cm]
        {\Large {\bfseries TIESIOGINIS IŠVEDIMAS PRODUKCIJŲ SISTEMOJE (PYTHON)}}
        \\[3cm]

        \hfill\begin{tabular}{@{}p{.55\linewidth}@{}}
             \large
             \emph{Autorius:}\\
             Informatikos (KM) bakalauro studijų programos\\
             {\fontlibertineⅣ} kurso studentas\\
             Simonas \textsc{Kazlauskas}\\
        \end{tabular}
        \vfill
        {\large Vilnius\\
         \today}
    \end{center}
\end{titlepage}

% \tableofcontents
% \clearpage

\pagenumbering{arabic}

\tableofcontents
\clearpage

\section{Įvadas}

Dalyko ,,Dirbtinis intelektas'' pirmasis laboratorinis darbas yra tiesioginio išvedimo
produkcijų sistemoje suprogramavimas pasirinkta programavimo kalba bei sukurtos programos
aprašymas. Šis dokumentas atlieka aprašo funkciją. Atliekant šią užduotį buvo naudotasi tik
informacija išdėstyta paskaitų metu bei šių paskaitų konspektu \cite{cyr08}.

\subsection{Užduotis}

Darbe nagrinėjama produkcijų sistema yra sudaryta iš:

\begin{enumerate}
    \item {\bf Produkcijų aibės.} Aibė produkcijų (produkcijos pavyzdys: $\pi1: \mathrm{A}, \mathrm{B}
        \rightarrow \mathrm{C}$) sudarytų iš konsekvento (rodyklės dešinėje pusėje; ${\pi1}$ atveju
        $\mathrm{C}$) ir antecedentų (rodyklės kairėje pusėje; $\pi1$ atveju $\mathrm{A}$ ir
        $\mathrm{B}$);
    \item {\bf Globalios duomenų bazės (GDB).} Aibės propozicinių kintamųjų laikomų teisingais;
    \item {\bf Tiesioginio išvedimo sistemos.} Algoritmo kuris nustato (jei egzistuoja) tokią
        produkcijų seką, kuri globalią duomenų bazę perveda į terminalinę būseną produkcijas
        perinkdamas iš eilės ir tikrindamas ar nagrinėjama produkcija gali būti pritaikyta esant
        dabartinei GDB.
\end{enumerate}

Tiesioginio išvedimo sistemos aprašymas iš esmės nusako užduoties tikslą: rasti produkcijų seką
kurią taikant GDB galima pervesti į terminalinę būseną.

\section{Tiesioginis išvedimas}

Tiesioginio išvedimo algoritmas veikia šiuo principu:

\begin{enumerate}

\item Iš eilės perrenkamos visos produkcijos;
\item Patikrinama ar visi nagrinėjamos produkcijos antecedentai yra globalioje duomenų bazėje;
\item Jei visi anticedentai yra GDB, nagrinėjamos produkcijos konsekventas yra įterpiamas į GDB ir
    patikrinama ar pasiekta terminalinė būsena. Perrinkimas kartojamas iš naujo;
\item Jei terminalinė būsena nepasiekta, o visos produkcijos perrinktos, tai ieškoma produkcijų
    seka neegzistuoja.
\end{enumerate}

\subsection{Pseudokodas}

\begin{minted}[escapeinside=||]{text}
forward_chain : [Production] |\rightarrow| [Var] |\rightarrow| Var  |\rightarrow| Maybe [Production]
forward_chain    productions    gdb     goal =
        let chain = []
{- 4 -} while any applicable productions
{- 1 -}   for production in productions
{- 2 -}     if antecedents of production |\subset| gdb then
{- 3 -}       gdb = gdb with consequent of production
              add production to chain
{- 3 -}       if goal in gdb then become (Some chain)
{- 4 -} become Nothing
\end{minted}

\subsection{Programos veikimas}

Programa parašyta Python programavimo kalbos 3 versija. Ją sudaro dvi pagrindinės dalys: įvesties
failą analizuojanti funkcija ir tiesioginės išvesties algoritmą atliekanti funkcija. Programa
paleidžiama \mintinline{sh}|python3 gdb.py FAILAS|, kur \mintinline{sh}|FAILAS| yra kelias iki failo.

Programos veikimui įvertinti buvo naudojami paskaitos metu pateikti ir autoriaus sugalvoti
pavyzdžiai. Juos atitinkantys failai yra, kartu su pačia programa, pateikti priedu prie šio aprašo.

\subsubsection{1 pavyzdys}

Pirmasis pavyzdys atitinka paskaitų metu pateiktą uždavinį. Šiame pavyzdyje yra ir atliekamų
produkcijų.

\paragraph{Įvesties failas}

\inputminted[texcomments]{text}{test1}

\paragraph{Programos išvestis}

\inputminted{text}{test1.out}

\paragraph{Semantinis grafas}


\begin{center}
\includegraphics[width=0.8\linewidth]{{{test1.gv}}}
\end{center}

\subsubsection{2 pavyzdys}

Antrąjame pavyzdyje produkcijos išdėstytos tokia tvarka, kad gautųsi ilga produkcijų seka.

\paragraph{Įvesties failas}

\inputminted[texcomments]{text}{test2}

\paragraph{Programos išvestis}

\inputminted{text}{test2.out}

\paragraph{Semantinis grafas}

\begin{center}
\includegraphics[width=\linewidth]{{{test2.gv}}}
\end{center}

\subsubsection{3 pavyzdys}

Trečiąjame pavyzdyje produkcijos yra tokios pačios kaip ir antrąjame, bet išdėstytos tokia tvarka,
kad produkcijų seka gautųsi trumpa.

\paragraph{Įvesties failas}

\inputminted[texcomments]{text}{test3}

\paragraph{Programos išvestis}

\inputminted{text}{test3.out}

\paragraph{Semantinis grafas}

\begin{center}
\includegraphics[width=0.6\linewidth]{{{test3.gv}}}
\end{center}

\subsubsection{4 pavyzdys}

4 pavyzdys iš esmės tikrina programos veikimą kai faktų aibėje jau egzistuoja tikslas.

\paragraph{Įvesties failas}

\inputminted[texcomments]{text}{test4}

\paragraph{Programos išvestis}

\inputminted{text}{test4.out}

\subsubsection{5 pavyzdys}

5 pavyzdyje tikrinamas programos veikimas kai terminalinė būsena neišvedama.

\paragraph{Įvesties failas}

\inputminted[texcomments]{text}{test5}

\paragraph{Programos išvestis}

\inputminted{text}{test5.out}

\section{Programos tekstas}

\inputminted{python}{gdb.py}

\section{Priedai}

Kartu su darbu yra pateikiama vieša Git repozitorija kurioje yra šio darbo išvesties failai.
Repozitorija pasiekiama adresu \url{https://gitlab.com/nagisa/forward-chaining}. Taip pat ją galima
nusiklonuoti naudojantis žemiau pateikta komanda:

\mint[linenos=false, frame=none]{sh}|git clone https://gitlab.com/nagisa/forward-chaining.git|.

\clearpage

\printbibliography

\end{document}
